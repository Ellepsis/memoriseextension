"use strict";

const SITE_IS_BLOCKED_INFO_BLOCK = "siteIsBlockedInfoBlock";
const SITE_NOT_BLOCKED_INFO_BLOCK = "siteNotBlockedInfoBlock";

class BlockedSitesService {

    isUrlRestricted(url) {
        chrome.runtime.sendMessage({
            type: "GET_BLOCKED_SITES"
        }, (res) => {
            if (res == null || res.result == null || !res.result.includes(url)) {
                document.getElementById(SITE_IS_BLOCKED_INFO_BLOCK).style.visibility = "hidden";
                document.getElementById(SITE_NOT_BLOCKED_INFO_BLOCK).style.visibility = "visible";
            } else {
                document.getElementById(SITE_NOT_BLOCKED_INFO_BLOCK).style.visibility = "hidden";
                document.getElementById(SITE_IS_BLOCKED_INFO_BLOCK).style.visibility = "visible";
            }
        });
    }

    isCurrentTabRestricted() {
        chrome.tabs.getSelected(null, (tab) =>{
            this.isUrlRestricted(tab)
        });
    }

    makeCurrentSiteRestricted(){
        chrome.tabs.query({'active': true, 'lastFocusedWindow': true, 'currentWindow': true}, function (tabs) {
            var url = tabs[0].url;
            console.log(url);
            chrome.runtime.sendMessage({
                type: "ADD_BLOCKED_SITE",
                data: {url}
            });
        });
    }

    init(){
        document.getElementById("addSiteToBlockedList").addEventListener('click', this.makeCurrentSiteRestricted);
        this.isCurrentTabRestricted();
    }
}

const blockedSitesService = new BlockedSitesService();
blockedSitesService.init();