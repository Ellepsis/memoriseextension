function showLoginForm() {
    window.location.href = "login.html";
}

function checkLoginStatus() {
    chrome.runtime.sendMessage({
        type: "CHECK_AUTH_STATUS"
    }, (res) => {
        console.log(res);
        if (res == null || res.result !== true) {
            showLoginForm();
        }
    });
}

window.onload = function () {
    checkLoginStatus();
    console.log("onload" + Date());
};