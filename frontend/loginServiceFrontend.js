function showMainForm() {
    window.location.href = "hello.html";
}

function processLoginData() {
    const password = document.getElementById("password").value;
    const username = document.getElementById("username").value;
    chrome.runtime.sendMessage({
        type: "USER_AUTH_DATA",
        data: {
            username: username,
            password: password
        }
    }, (res) => {
        console.log(res);
        if (res != null && res.result === true) {
            showMainForm();
        }
    });
}

document.getElementById('loginButton').onclick = processLoginData;