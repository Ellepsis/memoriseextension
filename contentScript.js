let didScroll = false;
const MY_ID_TAG_ATTRIBUTE = 'data-cxj_dus_dsl';
let idCounter = 0;

$(window).scroll(function () {
    didScroll = true;
});

function detectElements() {
    const time = new Date();
    let elements = $("*")
        .filter(":visible");
    const windowHeight = $(window).height();
    const windowWidth = $(window).width();
    elements = elements.filter(o => isElementVisible(elements[o], windowHeight, windowWidth));
    elements.each(o => generateIdsForElement(elements[o]));

    elements = getParentElements(elements);

    //const text = parseText(elements);
    sendToBackground(time, elements.map(element => element.outerHTML));
}

function generateIdsForElement(element) {
    const attribute = $(element).attr(MY_ID_TAG_ATTRIBUTE);
    if (attribute == null) {
        $(element).attr(MY_ID_TAG_ATTRIBUTE, idCounter++);
        element.childNodes.forEach(o => generateIdsForElement(o));
    }
}

function getParentElements(elements) {
    let parentElements = [];
    for (let elem of elements) {
        let isParent = true;
        const notParent = [];
        for (let parent of parentElements) {
            if ($.contains(parent, elem)) {
                isParent = false;
            }
            if ($.contains(elem, parent)) {
                notParent.push(parent);
            }
        }
        if (isParent) {
            parentElements.push(elem);
        }
        parentElements = parentElements.filter(o => !notParent.includes(o));
    }
    return parentElements;
}

setInterval(function () {
    if (didScroll) {
        didScroll = false;
        detectElements();
    }
}, 1000);


function sendToBackground(time, text) {
    try {
        chrome.runtime.sendMessage({
            type: "ELEMENT_DATA_MSG_TYPE",
            data: {
                dateTime: time.toJSON(),
                text: text
            }
        });
    } catch (err) {
        if (!err.message.startsWith("Extension context invalidated.")) {
            throw err;
        }
    }
}

Date.prototype.toJSON = function () {
    return this.toISOString();
};

function isElementVisible(el, windowHeight, windowWidth) {
    let boundingClientRect = el.getBoundingClientRect();
    if (el.childElementCount === 0) {
        return isAnyPartOfElementInViewport(boundingClientRect, windowHeight, windowWidth);
    } else {
        return isEntireElementInViewport(boundingClientRect, windowHeight, windowWidth);
    }
}

/**
 * Function returns true, if any part of element is presented on screen
 * @param elBoundingRect - rect of an element
 * @param windowHeight
 * @param windowWidth
 * @returns {boolean}
 */
function isAnyPartOfElementInViewport(elBoundingRect, windowHeight, windowWidth) {

    const vertInView = (elBoundingRect.top <= windowHeight) && ((elBoundingRect.top + elBoundingRect.height) >= 0);
    const horInView = (elBoundingRect.left <= windowWidth) && ((elBoundingRect.left + elBoundingRect.width) >= 0);

    return (vertInView && horInView);
}

/**
 * Function returns true, if entire element is inside viewport
 * @param elBoundingRect - rect of an element
 * @param windowHeight
 * @param windowWidth
 * @returns {boolean}
 */
function isEntireElementInViewport(elBoundingRect, windowHeight, windowWidth) {
    const vertInView = elBoundingRect.top >= 0 && ((elBoundingRect.top + elBoundingRect.height) <= windowHeight);
    const horInView = elBoundingRect.left >= 0 && ((elBoundingRect.left + elBoundingRect.width) <= windowWidth);
    return (vertInView && horInView);
}

function injectInputsListeners() {
    const inputs = document.getElementsByTagName('input');
    for (let i = 0; i <= inputs.length; i++) {
        let inputElement = inputs[i];
        if (inputElement == null) {
            continue;
        }

        let type = inputElement.getAttribute("type");
        if (type == null || type === "text") {
            inputElement.addEventListener('input', function (evt) {
                processInput(this.value, inputElement);
            });
        }
    }
}

injectInputsListeners();

function processInput(input, elem) {
    generateIdsForElement(elem);
    let attribute = elem.getAttribute(MY_ID_TAG_ATTRIBUTE);
    sendInputToBackground(new Date(), input, attribute);
    console.log("input: " + input);
}

function sendInputToBackground(time, input, elementId) {
    try {
        chrome.runtime.sendMessage({
            type: "ELEMENT_INPUT_MSG_TYPE",
            data: {
                dateTime: time.toJSON(),
                text: input,
                id: elementId
            }
        });
    } catch (err) {
        if (!err.message.startsWith("Extension context invalidated.")) {
            throw err;
        }
    }
}