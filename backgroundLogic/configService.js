var BLOCKED_SITES_LIST = "BLOCKED_SITES_LIST";

function addBlockedSite(siteName) {
    getBlockedSites((res) => {
        if (res == null) {
            res = [];
        }
        if (!res.includes(siteName)) {
            res.push(siteName);
            chrome.storage.sync.set({BLOCKED_SITES_LIST: res}, function () {
            });
        }
    })
}

function getBlockedSites(resCallback) {
    return chrome.storage.sync.get(BLOCKED_SITES_LIST, (res) => {
        if (Array.isArray(res)) {
            resCallback(res);
        } else {
            resCallback([]);
        }
    });
}

function removeBlockedSite() {
    getBlockedSites((res) => {
        if (res != null && !res.includes(siteName)) {
            res = res.filter(name => name !== siteName);
            chrome.storage.sync.set({BLOCKED_SITES_LIST: res}, function () {
            });
        }
    })
}