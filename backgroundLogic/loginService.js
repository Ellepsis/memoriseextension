const IS_USER_AUTHENTICATED_KEY = "IS_USER_AUTHENTICATED_KEY";

function login(username, password, callback) {
    $.ajax({
        url: host + "login",
        type: 'POST',
        data: {username: username, password: password},
        success: () => {
            setAuthorizationStatus(true);
            if (callback != null) {
                callback(true);
            }
        },
        error: (error) => {
            console.log(error);
            if (callback != null) {
                callback(false);
            }
        }
    })
}

function setAuthorizationStatus(isActive) {
    chrome.storage.sync.set({'IS_USER_AUTHENTICATED_KEY': isActive}, function () {
    });
}

function getAuthorizationStatus(resCallback) {
    return chrome.storage.sync.get(IS_USER_AUTHENTICATED_KEY, (res) => {
        if (res != null && res.IS_USER_AUTHENTICATED_KEY) {
            resCallback(true);
        }
        resCallback(false);
    });
}