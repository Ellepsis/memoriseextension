const ELEMENT_DATA_MSG_TYPE = "ELEMENT_DATA_MSG_TYPE";
const ELEMENT_INPUT_MSG_TYPE = "ELEMENT_INPUT_MSG_TYPE";
const USER_AUTH_DATA = "USER_AUTH_DATA";
const CHECK_AUTH_STATUS = "CHECK_AUTH_STATUS";
const GET_BLOCKED_SITES = "GET_BLOCKED_SITES";
const ADD_BLOCKED_SITE = "ADD_BLOCKED_SITE";

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    if (request.type != null) {
        if (request.type === ELEMENT_DATA_MSG_TYPE) {
            addElementsToMap(request.data.text, request.data.dateTime);
        }
        if (request.type === ELEMENT_INPUT_MSG_TYPE) {
            saveInputText(request.data.text, request.data.id);
        }
        if (request.type === USER_AUTH_DATA) {
            login(request.data.username, request.data.password, (res) => {
                sendResponse({result: res});
            });
            return true;
        }
        if (request.type === CHECK_AUTH_STATUS) {
            getAuthorizationStatus((res) => {
                sendResponse({result: res});
            });
            return true;
        }
        if (request.type === GET_BLOCKED_SITES) {
            getBlockedSites(res => {
                sendResponse({result: res});
            });
            return true;
        }
        if (request.type === ADD_BLOCKED_SITE) {
            addBlockedSite(request.data.site);
            sendResponse(true);
            return true;
        }
        sendResponse();
    }
});

chrome.tabs.onActivated.addListener(function () {
    const elements = getViewedElementsAndClearMaps();
    const inputs = getInputsAndClear();
    if (elements != null && elements.length > 0) {
        elements.forEach(elem => elem.convertElementToText());
        sendTextToServer(JSON.stringify(elements.filter(o => o.text != null && o.text.length > 0)));
    }
    if (inputs != null && inputs.length > 0) {
        sendInputToServer(JSON.stringify(inputs))
    }
});
