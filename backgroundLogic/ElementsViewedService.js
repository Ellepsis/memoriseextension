const MY_ID_TAG_ATTRIBUTE = 'data-cxj_dus_dsl';

let currentlyViewedElementsMap = new Map();
let previouslyViewedElementsMap = new Map();
let inputTextMap = new Map();

function authorize(response) {
    if (response.status != null && (response.status === 403 || response.status === 401)) {
        setAuthorizationStatus(false);
    }
}

function sendTextToServer() {
    sendToServer(data, host + "api/userText/newText");
}

function sendInputToServer(data) {
    sendToServer(data, host + "api/userText/newInputs");
}

function sendToServer(request, url) {
    $.ajax({
        url: url,
        type: 'POST',
        data: request,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: true,
        success: msg => console.log(msg),
        error: msg => {
            authorize(msg)
        }
    })
}

function addElementToMap(element, elementId, timestamp) {
    let savedElement = currentlyViewedElementsMap.get(elementId);

    //if an element in currently viewed, skip it
    //if element in previously viewed, update its timestamp
    if (savedElement == null) {
        savedElement = previouslyViewedElementsMap.get(elementId);
        if (savedElement != null) {
            savedElement.addViewStartTimestamp(timestamp);
            previouslyViewedElementsMap.delete(elementId);
            currentlyViewedElementsMap.set(elementId, savedElement);
        }
    } else {
        return;
    }

    //if element doesn't exist in any map, create that
    if (savedElement == null) {
        const childIds = [];
        element[0].childNodes.forEach(o => {
            if (typeof o.getAttribute === "function") {
                childIds.push(o.getAttribute(MY_ID_TAG_ATTRIBUTE));
            }
        });
        let elementToSave = new ViewedElement(elementId, element, childIds);
        elementToSave.addViewStartTimestamp(timestamp);
        currentlyViewedElementsMap.set(elementId, elementToSave);
    }
}

function removeElementsOutOfScope(elementsKeys, timestamp) {
    const viewedKeys = Array.from(currentlyViewedElementsMap.keys());
    const outOfScopeKeys = viewedKeys.filter((key) => !elementsKeys.includes(key));
    outOfScopeKeys.forEach(key => {
        const element = currentlyViewedElementsMap.get(key);
        element.addEndTimestamp(timestamp);
        previouslyViewedElementsMap.set(key, element);
        currentlyViewedElementsMap.delete(key);
    })
}

function addElementsToMap(elements, timestamp) {
    let domElements = elements.map(o => $.parseHTML(o));
    const newElementsKeys = [];
    domElements.forEach(element => {
        const attribute = element[0].getAttribute(MY_ID_TAG_ATTRIBUTE);
        if (attribute != null) {
            addElementToMap(element, attribute, timestamp);
            newElementsKeys.push(attribute);
        }
    });
    removeElementsOutOfScope(newElementsKeys, timestamp);
}

function getViewedElementsAndClearMaps() {
    const currentTime = new Date();
    const elementsToSend = [];
    currentlyViewedElementsMap.forEach((val) => {
        val.addEndTimestamp(currentTime);
        elementsToSend.push(val);
    });
    previouslyViewedElementsMap.forEach((val) => {
        elementsToSend.push(val);
    });
    currentlyViewedElementsMap.clear();
    previouslyViewedElementsMap.clear();
    return elementsToSend;
}

function saveInputText(inputValue, elementId) {
    if (inputValue == null) {
        return;
    }
    let inputs = inputTextMap.get(elementId);
    if (inputs == null) {
        inputs = [];
        inputTextMap.set(elementId, inputs);
    }
    let previousVal = "";
    if (inputs.length !== 0) {
        previousVal = inputs.pop();
    }
    if (previousVal == null || inputValue.startsWith(previousVal)) {
        inputs.push(inputValue);
    } else if (previousVal.startsWith(inputValue)) {
        inputs.push(previousVal);
    } else {
        inputs.push(previousVal);
        inputs.push(inputValue);
    }
}

function getInputsAndClear() {
    let res = [];
    inputTextMap.forEach(function (value) {
        res = res.concat(value)
    });
    inputTextMap.clear();
    return res;
}
