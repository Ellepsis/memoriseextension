class ViewedElement {

    constructor(id, element, containsElementsIds) {
        this.id = id;
        this.element = element;
        this.timeMarks = [];
        this.childElementIds = containsElementsIds;
    }

    getElement() {
        return this.element;
    }

    addViewStartTimestamp(timestamp) {
        this.timeMarks.push(new ViewDuration(timestamp));
    }

    addEndTimestamp(timestamp) {
        const endTime = this.timeMarks[this.timeMarks.length - 1].endTime;
        if (endTime != null) {
            this.timeMarks[this.timeMarks.length - 1].endTime = timestamp;
        }
    }

    /**
     * Method converts element text and all of its nodes to string array
     * and sets the class field to converted value, to be sent to the server.
     */
    convertElementToText() {
        this.text = this.element[0].textContent.split("\n").map(o => o.trim()).filter(o => o.length > 0);
        delete this.element;
    }
}